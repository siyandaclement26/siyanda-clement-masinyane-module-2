import 'dart:ffi';

void main() {
  var app = App("Shyft", 'Financial Solution Accolade', 'Standard Bank', 2017);
  print("App Name :" + app.appName.toUpperCase());
  print("Sector/Category :" + app.sectorOrcategory);
  print("Name of Developer is :" + app.developer);
  print("The year it won MTN Business App of the Year Awards is :" +
      app.Year_Won_Awards.toString());

  print("");

  var app2 = App("Khula Farmers APP", 'Best Agriculture Solution',
      'Karidas Tshintsholo and Matthew Piper', 2018);

  print("App Name :" + app2.appName.toUpperCase());
  print("Sector/Category :" + app2.sectorOrcategory);
  print("Name of Developer is :" + app2.developer);
  print("The year it won MTN Business App of the Year Awards is :" +
      app2.Year_Won_Awards.toString());
}

class App {
  String appName = "";
  String sectorOrcategory = '';
  String developer = '';
  int Year_Won_Awards = 0;
  App(String appName, String sectorOrcategory, String developer,
      int Year_Won_Awards) {
    this.appName = appName;
    this.sectorOrcategory = sectorOrcategory;
    this.developer = developer;
    this.Year_Won_Awards = Year_Won_Awards;
  }
}
